#coding: UTF-8

#入力フォルダ内のフォルダを次々見て、中のcsvを処理させて、出力フォルダに入力フォルダ内のディレクトリを再現して、中に1秒ごとにしたものを配置する
#入力、出力の引数は絶対パス


import sys
import os
import subprocess
import shlex

if __name__ == "__main__":
	#print("\nExplore directory: {input}".format(input = sys.argv[1]))
	inputdirectories = os.listdir(sys.argv[1])

	for directory in inputdirectories:
		#ファイルはスキップ(.DS_Store対策でもある)
		if not os.path.isdir(os.path.join(sys.argv[1], directory)):
			continue
		#出力ディレクトリがすでに存在するときはスキップ
		elif os.path.isdir(os.path.join(sys.argv[2], directory)):
			print("ディレクトリが存在するのでスキップ: {0}".format(os.path.join(sys.argv[2], directory)))
			continue

		#出力ディレクトリを生成
		outputdirectory = os.path.join(sys.argv[2], directory)
		os.mkdir(outputdirectory)

		#フォルダ内のcsvをリストアップ
		csvlist = os.listdir(os.path.join(sys.argv[1], directory))
		for csvfile in csvlist:
			#macが作るゴミは飛ばす
			if "coordinate-correct.txt" != csvfile:
				continue

			#シェルコマンドを呼ぶ
			command_line = "python3 {0} {1} {2}".format(os.path.join(os.path.abspath(os.path.dirname(__file__)),"normalize_second.py"), os.path.join(sys.argv[1], directory, csvfile), os.path.join(outputdirectory, csvfile))
			commandresult = subprocess.check_call(shlex.split(command_line))
