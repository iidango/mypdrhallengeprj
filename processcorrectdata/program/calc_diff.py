#calc_diff.py
#
#正解データと推定データの誤差を計算する
#
#Usage: calc_diff.py correct_data_filepath estimated_data_filepath output_filepath

import csv
import sys
import math
import os

if __name__=='__main__':

    #ファイル読み込み
    #print("\nInput : {input}".format(input = sys.argv[1]))
    #print("Input : {input}".format(input = sys.argv[2]))
    cor_f = open(sys.argv[1])
    est_f = open(sys.argv[2])
    cor_data = csv.reader(cor_f)
    est_data = csv.reader(est_f)

    #誤差計算して出力を生成
    output_list = []
    for est_row in est_data:
        try:
            cor_row = next(cor_data)
            cor_row_cp = cor_row
        except StopIteration:
            cor_row = cor_row_cp
            cor_row[0] = float(cor_row[0]) + 1.0
        
        #タイムスタンプが一致しているかチェック
        if float(cor_row[0])!=float(est_row[0]):
            break

        #出力の生成
        output_row = []
        output_row.append(float(est_row[0]))   #タイムスタンプをいれる
            
        #誤差をユークリッド距離で計算
        diff = 0
        for j in range(1, 4):
            diff += pow(float(cor_row[j]) - float(est_row[j]), 2)
        diff = math.sqrt(diff)
        output_row.append(diff)

        #print(output_row)
        
        #出力行を追加
        output_list.append(output_row)
    
    cor_f.close()
    est_f.close()
            
    #出力
    #print("Output: {output}".format(output = sys.argv[3]))
    #まずディレクトリを生成
    dir_path = os.path.dirname(sys.argv[3])
    if not os.path.exists(dir_path):
        os.system("mkdir -p "+dir_path)
    with open(sys.argv[3], 'w') as f:
        writer = csv.writer(f, lineterminator='\n')
        writer.writerows(output_list)


