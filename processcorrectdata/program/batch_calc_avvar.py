#batch_calc_avvar.py
#
#calc_avvar.pyを使ってフォルダ内の誤差csvの平均，分散をまとめたファイルを生成する
#input_dirpath内のcsvを検索して，その平均，分散をoutput_filepathにまとめる
#outputfile: csv,avg,var,size
#
#Usage: batch_calc_avvar.py input_dirpath output_filepath

import csv
import sys
import os

#ファイル一覧のイテレータを返す
def filesIterator(rootDir):
    for path, dirs, files in os.walk(rootDir):
        for filePath in map(lambda x: os.path.join(path, x), files):
            yield filePath

if __name__=='__main__':

    if os.path.exists(sys.argv[2]):
        os.remove(sys.argv[2])

    #ファイル一覧を取得
    filePath = filesIterator(sys.argv[1])
    for file in filePath:
        if file.endswith("csv"):
            cmd = "python3 calc_avvar.py "+file+" "+sys.argv[2]     #実行コマンド
            #実行
            os.system(cmd)
            #print(cmd)



