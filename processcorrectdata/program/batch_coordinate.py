#batch_coordinate.py
#
#input_dirpath内を検索して, .pyを実行し，output_filepathにディレクトリ構造を再現して出力する
#pathは絶対で指定
#
#Usage: batch_coordinate.py input_dirpath input_filename output_dirpath output_filename

import csv
import sys
import os
import shutil

#ファイル一覧のイテレータを返す
def filesIterator(rootDir):
    for path, dirs, files in os.walk(rootDir):
        for filePath in map(lambda x: os.path.join(path, x), files):
            yield filePath

if __name__=='__main__':

    if os.path.exists(sys.argv[3]):
        shutil.rmtree(sys.argv[3])

    #ファイル一覧を取得
    filePath = filesIterator(sys.argv[1])
    for file in filePath:
        if file.endswith(sys.argv[2]):
            dirstruct = file.split(sys.argv[1])     #inputのディレクトリ構造を再現
            input_path = file       #inputのファイルパスを生成
            output_path = sys.argv[3] + os.path.dirname(dirstruct[1]) + "/" + sys.argv[4]    #outputのファイルパスを生成
            cmd = "python3 " + "normalize_second.py" + " "+input_path+" "+output_path     #実行コマンド
            #実行
            os.system(cmd)
            #print(cmd)




