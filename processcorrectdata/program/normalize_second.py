#coding: UTF-8

#入力ファイル(csv)を見て、時間を1秒ごとに直して、引数に与えた出力ファイルに吐き出す
#入力、出力は絶対パス指定
#usage:
#normalize_second.py <入力ファイル> <出力ファイル>
#normalize_second.py /hoge/piyo/foo.csv /hoge/fuga/bar.csvみたいな感じ


import csv
import sys
import os
import math
import copy


if __name__ == "__main__":
	#入力ファイルが存在するかどうかの確認
	#print("\nInput : {input}".format(input = sys.argv[1]))
	#入力パスが不正なものだと死ぬ
	if not os.path.isfile(sys.argv[1]):
		print("{input} does not exist.".format(input = sys.argv[1]))
		exit()

	#読み込み
	with open(sys.argv[1], 'r') as f:
		reader = csv.reader(f)
		#前回の行を保存しておく
		prevrow = ""

		#出力で使うやつ
		normalizedrows = []

		for row in reader:
			#最初の1行だけは直接normalizedrowに足してよい
			if prevrow  == "":
				normalizedrows.append(row)
				prevrow = row
				continue

			#2行目以降
			if (math.floor(float(prevrow[0]))) != (math.floor(float(row[0]))):
				#時間の差分
				timegap = float(row[0]) - float(prevrow[0])
				#timegap内での速度[x,y,z]です
				averagespeed = [(float(row[1]) - float(prevrow[1]))/timegap, (float(row[2]) - float(prevrow[2]))/timegap, (float(row[3]) - float(prevrow[3]))/timegap]

				#2秒以上間に入るものがあれば、その分だけ回す
				#issiさんがUnixTimeのままのファイルを吐くようにした場合，修正する必要がある
				for i in range(math.floor(float(row[0])) - math.floor(float(prevrow[0]))):
					#normalizedrowsに追加する1行
					appendrow =[(i + 1) + float(math.floor(float(prevrow[0]))), 0, 0, 0]

					#時間ぴったりのときのx,y,zを生成: ぴったりvalue = 前のvalue + 位置差分*(ぴったり時間との差分/timegap)
					appendrow[1] = round(float(prevrow[1]) + averagespeed[0]*(appendrow[0] - float(prevrow[0])), 2)
					appendrow[2] = round(float(prevrow[2]) + averagespeed[1]*(appendrow[0] - float(prevrow[0])), 2)
					appendrow[3] = round(float(prevrow[3]) + averagespeed[2]*(appendrow[0] - float(prevrow[0])), 2)

					normalizedrows.append(appendrow)

			prevrow = row
			#↑ここまで for row in readerの中身

	#もしも一番最後に初期位置に戻ってきていなかったら
	if not (float(normalizedrows[len(normalizedrows)-1][1]) == float(normalizedrows[0][1]) and float(normalizedrows[len(normalizedrows)-1][2]) == float(normalizedrows[0][2]) and float(normalizedrows[len(normalizedrows)-1][3]) == float(normalizedrows[0][3])):
		#最後に最初の座標に戻るように一行追加する
		appendrow = copy.deepcopy(normalizedrows[0])
		appendrow[0] = normalizedrows[len(normalizedrows)-1][0] + 1
		normalizedrows.append(appendrow)

	#print("Output: {output}".format(output = sys.argv[2]))

	#↓出力
	dir_path = os.path.dirname(sys.argv[2])
	#まずディレクトリを生成
	if not os.path.exists(dir_path):
		os.system("mkdir -p "+dir_path)

	with open(sys.argv[2], 'w') as f:
		writer = csv.writer(f)

		writer.writerows(normalizedrows)
