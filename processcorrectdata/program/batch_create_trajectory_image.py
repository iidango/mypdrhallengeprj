#batch_create_trajectory_image.py
#
#coordinate_abs_dirpath, estimated_data_path内を検索して, create_trajectory_image.pyを実行し，output_filepathにディレクトリ構造を再現して出力する
#pathは絶対で指定
#
#Usage: batch_create_trajectory_image.py coordinate_abs_filepath inpupt_data_dirpath input_data_filename image_filepath output_filepath output_filename


import csv
import sys
import os
import shutil

#ファイル一覧のイテレータを返す
def filesIterator(rootDir):
    for path, dirs, files in os.walk(rootDir):
        for filePath in map(lambda x: os.path.join(path, x), files):
            yield filePath

if __name__=='__main__':

    if os.path.exists(sys.argv[5]):
        shutil.rmtree(sys.argv[5])
    #ファイル一覧を取得
    filePath = filesIterator(sys.argv[2])
    for file in filePath:
        if file.endswith(sys.argv[3]):
            dirstruct = file.split(sys.argv[2])     #ディレクトリ構造を再現
            coordinate_abs_filepath = sys.argv[1]       #coordinate_absのファイルパスを生成
            input_data_path = file    #input_dataのファイルパスを生成
            image_filepath = sys.argv[4]
            output_path = sys.argv[5] + os.path.dirname(dirstruct[1]) + "/" + sys.argv[6]    #outputのファイルパスを生成
            cmd = "python3 " + "create_trajectory_image.py" + " "+coordinate_abs_filepath+" "+input_data_path+" "+image_filepath+" "+output_path     #実行コマンド
            #実行
            os.system(cmd)
            #print(cmd)




