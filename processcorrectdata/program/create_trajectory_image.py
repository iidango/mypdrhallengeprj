#!/usr/bin/env python
# -*- coding: utf-8 -*-

from PIL import Image
from PIL import ImageDraw
import numpy as np
import csv
import math
import sys
import os

# 定数
IMAGE_FILE_EXTENSION = "-trajectory.png"
POINT_COLOR = (255,0,0)
TRIANGLE_COLOR = (0,0,255)
R = 2 # 点の大きさ


'''
移動軌跡を示す画像ファイルを作成します．

入力
route_file：正解経路データ[~coordinate-px-abs.txt]
cod_file  ：座標データ[route~_device~_id~_user~.csv]
image_file：フロア画像ファイル

出力
画像ファイル："[座標ファイル名]-trajecroty.png"

'''

def draw_trajectory(route_file="route1-coordinate-px-abs.txt",cod_file="route1_device_id3_user3.csv",image_file='8F-floormap.png',output_file="trajectory.png"):

	# 画像の読み込み
	img = Image.open(image_file)
	if img.mode != 'RGB':
		img = img.convert('RGB')

	# 推定座標データの読み込み
	dataList = read_csv_file(cod_file)
	# 経路情報の読み込み，初期状態を取得
	x,y,rot = get_initial_state(route_file)
	# 座標を変換
	p_list=convert_to_pixel(dataList,x,y,rot)

	# 画像に軌跡を描画
	# スタート，ゴールだけ点，あとは三角形
	for i in range(len(p_list)):
		if i > 0 and i < len(p_list)-1:
			img = draw_triangle(img,p_list[i+1][1],p_list[i+1][2],p_list[i][1],p_list[i][2])
		else:
			img = draw_point(img,p_list[i][1],p_list[i][2])

	# 画像を保存
	#output_file = cod_file.split(".")[0] + IMAGE_FILE_EXTENSION
	dir_path = os.path.dirname(output_file)
	#まずディレクトリを生成
	if not os.path.exists(dir_path):
		os.system("mkdir -p "+dir_path)
	img.save(output_file)
	# img.show()

# 入力画像に点を描画します
def draw_point(img,x,y):
	draw = ImageDraw.Draw(img)
	draw.ellipse(((x-R,y-R),(x+R,y+R)),outline=POINT_COLOR ,fill= POINT_COLOR)

	return img

# 入力画像に三角形を描画します
def draw_triangle(img,x,y,x_p,y_p):
	draw = ImageDraw.Draw(img)
	# 基本形状
	points = [[-4,-3],[-4,3],[8,0]]
	tuples=[]
	
	# 基本形状を回転させ，進行方向に合わせます
	rot = get_rotation(x,y,x_p,y_p)
	for p in points:
		m = rot * np.array([ [p[0]],[p[1]]])
		tuples.append((x_p + int(m[0]),y_p + int(m[1])))
	
	draw.polygon(tuples, fill= TRIANGLE_COLOR, outline=TRIANGLE_COLOR)
	
	return img

# 回転方向を取得します
def get_rotation(x,y,x_p,y_p):
	# 画像に座標系を合わせるため，-をかける
	theta = -math.atan2(y-y_p, x-x_p)
	
	# 回転方向
	rot = np.matrix((
		(np.cos(theta),np.sin(theta)),
		(-np.sin(theta),np.cos(theta))
	))

	return rot

# csv形式のファイルを読み込み,リスト形式で返します
def read_csv_file(input_file):
	dataList =[]
	with open(input_file,'rU') as f:
		reader = csv.reader(f)

		for row in reader:
			dataList.append(row)
	return dataList

# 座標データをpixel単位に変換します
def convert_to_pixel(data_list,x_0,y_0,rot):

	p_list = []
	for row in data_list:
		# 座標 → pixel
		row[1] = int(10*float(row[1]))
		row[2] = int(-10*float(row[2]))
		row[3] = int(10*float(row[3]))

		# オフセット，回転方向を追加
		m = rot*np.array([ [row[1]],[row[2]] ])
		row[1] = x_0 + int(m[0])
		row[2] = y_0 + int(m[1])

		p_list.append(row)

	return p_list

# 初期状態を取得
def get_initial_state(input_file):
	dataList = read_csv_file(input_file)
	x_0,y_0 = int(float(dataList[0][0])),int(float(dataList[0][1]))
	x,y = int(float(dataList[1][0])),int(float(dataList[1][1]))

	rot = get_rotation(x,y,x_0,y_0)

	return x_0,y_0,rot

#####################################################################

if __name__ == '__main__':
	if len(sys.argv) != 5:
		draw_trajectory()
	else:
		draw_trajectory(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
