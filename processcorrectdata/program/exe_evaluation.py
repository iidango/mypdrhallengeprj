#すべてbatchファイルを統括して実行する
#pdrdataがあるディレクトリで実行

import os
import sys
import json

ALL_ROUTE = ["route1", "route3", "route4", "route5", "route6"]

if __name__=='__main__':

    if len(sys.argv) != 2:
        print("Usage: python3 exe_evaliation.py <eval_setting_file>")
        exit()

    #設定ファイルの読み込み
    with open(sys.argv[1], 'r') as f:
        setting = json.load(f)

    #各パラメータをセットしていく
    root = setting['root']
    origin_correct_filename = setting['origin_correct_filename']
    origin_estimate_filename = setting['origin_estimate_filename']
    coordinate_correct_1sec_filename = setting['coordinate_correct_1sec_filename']
    coordinate_estimate_1sec_filename = setting['coordinate_estimate_1sec_filename']
    error_filename = setting['error_filename']

    #routeの設定
    routein = setting['route']
    routes = []
    if routein == "all":
        #すべてのroute
        for rt in ALL_ROUTE:
            routes.append("/"+rt)
    elif routein != "overall":
        #routeを","区切りで指定できる
        for rt in routein.split(","):
            routes.append("/"+rt)
    else:
        #"overall"
        routes.append("")
    
    #各routeに対して処理を行っていく
    for route in routes:
        origin_dirpath = root + "/" + setting['origin_dirpath'] + route
        coordinate_correct_1sec_dirpath = root + "/" + setting['coordinate_correct_1sec_dirpath'] + route
        coordinate_estimate_1sec_dirpath = root + "/" + setting['coordinate_estimate_1sec_dirpath'] + route
        error_dirpath = root + "/" + setting['error_dirpath'] + route
        error_collection_filepath = root + route + "/" + setting['error_collection_filepath']
        evalation_result_filepath = root + route + "/" + setting['evalation_result_filepath']
        trajectory_image_dirpath = root + "/" + setting['trajectory_image_dirpath'] + route

        #正解データの1sごとのファイルを生成
        print("coordinating coordinate correct 1sec file...")
        os.system("python3 batch_coordinate.py "+origin_dirpath+" "+origin_correct_filename+" "+coordinate_correct_1sec_dirpath+" "+coordinate_correct_1sec_filename)

    
        #推定データの1sごとのファイルを生成
        print("coordinating coordinate estimate 1sec file...")
        os.system("python3 batch_coordinate.py "+origin_dirpath+" "+origin_estimate_filename+" "+coordinate_estimate_1sec_dirpath+" "+coordinate_estimate_1sec_filename)

        #正解データと推定データから誤差を計算する
        print("calculating error file...")
        os.system("python3 batch_calc_diff.py "+coordinate_correct_1sec_dirpath+" "+coordinate_correct_1sec_filename+" "+coordinate_estimate_1sec_dirpath+" "+coordinate_estimate_1sec_filename+" "+error_dirpath+" "+error_filename)

        #誤差ファイルをひとつにまとめる
        print("collecting error file...")
        os.system("python3 batch_calc_avvar.py "+error_dirpath+" "+error_collection_filepath)

        #全体の誤差を計算する
        print("evalating whole algorithm...\n")
        os.system("python3 eval_whole_alg.py "+error_collection_filepath+" "+evalation_result_filepath)
        print("\n")
    

    #軌跡の可視化
    if setting['trajectory_flag'] == "0":
        exit()
    print("creating trajectory image file...")
    
    #"overallだったら全部のrouteを可視化
    if routein == "overall":
        routes = []
        #すべてのroute
        for rt in ALL_ROUTE:
            routes.append("/"+rt)

    floor_image_filepath = setting['floor_image_filepath']
    coordinate_abs_dirpath = setting['coordinate_abs_dirpath']
    coordinate_abs_filename = setting['coordinate_abs_filename']
    trajectory_image_filename = setting['trajectory_image_filename']

    for route in routes:
        coordinate_abs_filepath = coordinate_abs_dirpath + route + "/" + coordinate_abs_filename
        coordinate_estimate_1sec_dirpath = root + "/" + setting['coordinate_estimate_1sec_dirpath'] + route
        trajectory_image_dirpath = root + "/" + setting['trajectory_image_dirpath'] + route

        #軌跡を可視化した画像を生成する
        os.system("python3 batch_create_trajectory_image.py "+coordinate_abs_filepath+" "+coordinate_estimate_1sec_dirpath+" "+coordinate_estimate_1sec_filename+" "+floor_image_filepath+" "+trajectory_image_dirpath+" "+trajectory_image_filename)
