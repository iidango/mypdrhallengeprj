#calc_avvar.p
#
#誤差csvを読み込んで誤差の平均分散を計算する
#出力は"平均,分散"
#
#Usaga:
#   1番目(0から数えて)の要素の平均分散   
#       calc_avvar.py input_filepath output_filepath
#   n番目(0から数えて)の要素の平均分散
#       calc_avvar.py input_filepath output_filpath n

import csv
import sys
import math
import os

if __name__=='__main__':

    #ファイル入力だけなら2番目の要素を計算
    #要素の番号を指定したければ第2引数に指定
    if len(sys.argv)-1 == 2:
        index = 1
    else:
        index = int(sys.argv[3])

    #ファイル読み込み
    #print("\nInput : {input}".format(input = sys.argv[1]))
    in_f = open(sys.argv[1])
    in_data = csv.reader(in_f)

    #値の読み込み
    in_list = []
    for in_row in in_data:
        in_list.append(float(in_row[index]))

    #平均を計算
    adv = sum(in_list)/len(in_list)
    
    #分散を計算
    var = 0
    for num in in_list:
        var += pow(adv-num, 2)
    var /= len(in_list)

    in_f.close()

    #出力
    #print("Output: {output}".format(output = sys.argv[2]))
    output_list = [sys.argv[1], adv, var, len(in_list)]
    #まずディレクトリを生成
    dir_path = os.path.dirname(sys.argv[2])
    if not os.path.exists(dir_path):
        os.system("mkdir -p "+dir_path)
    #print(output_list)
    with open(sys.argv[2], 'a') as f:
        writer = csv.writer(f, lineterminator='\n')
        writer.writerow(output_list)



