#batch_calc_diff.py
#
#correct_data_dirpath, estimated_data_path内を検索して, calc_diff.pyを実行し，output_filepathにディレクトリ構造を再現して出力する
#pathは絶対で指定
#
#Usage: batch_calc_diff.py correct_data_dirpath correct_data_filename estimated_data_dirpath estimated_data_filename output_filepath output_filename

import csv
import sys
import os
import shutil

#ファイル一覧のイテレータを返す
def filesIterator(rootDir):
    for path, dirs, files in os.walk(rootDir):
        for filePath in map(lambda x: os.path.join(path, x), files):
            yield filePath

if __name__=='__main__':

    if os.path.exists(sys.argv[5]):
        shutil.rmtree(sys.argv[5])
    #ファイル一覧を取得
    filePath = filesIterator(sys.argv[1])
    for file in filePath:
        if file.endswith(sys.argv[2]):
            dirstruct = file.split(sys.argv[1])     #inputのディレクトリ構造を再現
            correct_data_path = file       #correct_dataのファイルパスを生成
            estimated_data_path = sys.argv[3] + os.path.dirname(dirstruct[1]) + "/" + sys.argv[4]    #estimated_dataのファイルパスを生成
            output_path = sys.argv[5] + os.path.dirname(dirstruct[1]) + "/" + sys.argv[6]    #outputのファイルパスを生成
            cmd = "python3 " + "calc_diff.py" + " "+correct_data_path+" "+estimated_data_path+" "+output_path     #実行コマンド
            #実行
            os.system(cmd)
            #print(cmd)




