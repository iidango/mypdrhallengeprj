#coding: UTF-8


#inputは絶対パスで与える
#inputに使うファイルの例
# ファイル名, 平均, 分散, 行数になっている
# foo.csv, 41.3457321, 10.865211, 20
# bar.csv, 62.120495, 20.7655433, 10
# baz.csv, 19.6554321467, 4.2356632, 30


import sys
import os
import csv
import math



if __name__ == "__main__":
	#print("\nInput file: {0}".format(sys.argv[1]))


	#重み付けに使う(最終的には全行数になる)
	weightbase = 0
	#重み付き平均
	weightedave = 0.0
	#重み付き標準偏差
	weightedsd = 0.0


	with open(sys.argv[1], 'r') as f:
		reader = csv.reader(f)

		for row in reader:
			weightbase += int(row[3], 10)


	with open(sys.argv[1], 'r') as f:
		reader = csv.reader(f)

		for row in reader:
			weightedave += float(row[1]) * float(int(row[3])/weightbase)
			weightedsd += math.sqrt(float(row[2])) * float(int(row[3])/weightbase)


	print("Weighted Average: {0}".format(weightedave))
	print("Weighted StdDev: {0}".format(weightedsd))

	#↓出力
	dir_path = os.path.dirname(sys.argv[2])
	output_list = [weightedave, weightedsd]
	#まずディレクトリを生成
	if not os.path.exists(dir_path):
		os.system("mkdir -p "+dir_path)

	with open(sys.argv[2], 'w') as f:
		writer = csv.writer(f)
		writer.writerow(output_list)
