programs for evaluation and trajectory visualization

* run environment
	python3
	(required python modules numpy and PIL)

* run command example
	python3 exe_evalation.py TestData_evalsetting.json

Input evalsetting file into 1st command line arguments.
Make evalsetting file(JSON) for evaluation before run our program. 
Please refer to TestData_evalsetting.json

[TestData_evalsetting.json]
{	"root" : "/Users/iida/Documents/PDRChallenge/myprogram/processcorrectdata/pdrdata", 
    "route" : "overall",
    "origin_dirpath" : "Testdata", 
    "origin_correct_filename" : "coordinate-correct.txt", 
    "origin_estimate_filename" : "coordinate-correct.txt", 
    "coordinate_correct_1sec_dirpath" : "coordinate_correct_1sec", 
    "coordinate_correct_1sec_filename" : "coordinate_correct_1sec.csv", 
    "coordinate_estimate_1sec_dirpath" : "coordinate_estimate_1sec", 
    "coordinate_estimate_1sec_filename" : "coordinate_estimate_1sec.csv", 
    "error_dirpath" : "error", 
    "error_filename" : "error.csv", 
    "error_collection_filepath" : "error_collection.csv", 
    "evalation_result_filepath" : "evalation_result.csv", 
    "trajectory_flag" : "1",
    "floor_image_filepath" : "/Users/iida/Documents/PDRChallenge/myprogram/processcorrectdata/program/8F-floormap.png",
    "coordinate_abs_dirpath" : "/Users/iida/Documents/PDRChallenge/myprogram/processcorrectdata/program/correct_coordinate_abs",
    "coordinate_abs_filename" : "coordinate-abs.txt",
    "trajectory_image_dirpath" : "trajectory",
	"trajectory_image_filename" : "trajectory.png"}

"root":"overall" -> evaluate algorithm
"root":"all" -> evaluate all route
"root":"route1,route4" -> evaluate route1,2 

-----------------------------

exe_evalation.py
	run ohter batch file
batch_coordinate.py
	run normalize_second.py to coordinate correct data and estimated data
batch_calc_diff.py
	run calc_diff.py to calc error.
batch_calc_avvar.py
	run calc_avvar.py to calc error average and varience
eval_whole_alg.py
	calc whole average and varience


