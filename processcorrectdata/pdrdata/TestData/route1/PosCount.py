# -*- coding: utf-8 -*-

import csv
import os
import sys

#read_dir = sys.argv[1]
read_dir = "0804"
write_f_name = "pos_count.csv"
TAG = "POS"


# 対象ディレクトリ内のすべてのファイル名取得
os.chdir(read_dir)
f_list =os.listdir(os.getcwd())

# 書き出しのディレクトリの確認
if not os.path.isdir(write_dir):
	os.mkdir(write_dir)

# ファイルを読み込み
lists = []
for f_name in f_list:
	# logじゃなかったら飛ばす
	if not f_name.endswith("log"):
		continue

	print("read " + f_name)
	with open(f_name,'r') as f:
		reader = csv.reader(f)
		count = 0
		list =[]
		list.append(f_name)

		# タグをカウント
		for row in reader:
			if TAG in row[0]:
				count+=1
				print(row)
		print(count)
		list.append(count)
		lists.append(list)

# ファイルに書き出し
with open(read_dir + write_f_name,'w+') as f:
	writer = csv.writer(f,lineterminator = '\n')
	writer.writerows(lists)



